<?php
function plusMinus($arr) {
    $totalCount = count($arr);
    $positiveCount = 0;
    $negativeCount = 0;
    $zeroCount = 0;
    
    foreach ($arr as $num) {
        if ($num > 0) {
            $positiveCount++;
        } elseif ($num < 0) {
            $negativeCount++;
        } else {
            $zeroCount++;
        }
    }
    
    printf("%.6f\n%.6f\n%.6f", ($positiveCount / $totalCount), ($negativeCount / $totalCount), ($zeroCount / $totalCount));
}