<?php
function countApplesAndOranges($s, $t, $a, $b, $apples, $oranges) {
    $appleHouseCount = 0;
    $orangeHouseCount = 0;
    
    foreach ($apples as $appleDist) {
        $applePos = $a + $appleDist;
        if ($applePos >= $s && $applePos <= $t) {
            $appleHouseCount++;
        }
    }
    
    foreach ($oranges as $orangeDist) {
        $orangePos = $b + $orangeDist;
        if ($orangePos >= $s && $orangePos <= $t) {
            $orangeHouseCount++;
        }
    }
    
    printf("%u\n%u\n", $appleHouseCount, $orangeHouseCount);
}