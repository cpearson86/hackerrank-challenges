<?php

function compareTriplets($a, $b) {
    $scores = [0, 0];
    
    foreach ($a as $key => $value) {
        if ($value > $b[$key]) {
            $scores[0]++;
        } elseif ($value < $b[$key]) {
            $scores[1]++;
        }
    }

    return $scores;
}