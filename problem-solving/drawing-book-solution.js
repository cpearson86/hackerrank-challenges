function pageCount(n, p) {
    // If the book has an even number of pages, the last page is blank, so we ignore it
    if ((n % 2) == 0) {
        n++;
    }
    
    const forwardTurns = Math.floor(p / 2);
    const backwardTurns = Math.floor((n - p) / 2);
    
    return Math.min(forwardTurns, backwardTurns);
}