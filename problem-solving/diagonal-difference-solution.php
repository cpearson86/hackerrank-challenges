<?php

function diagSum($arr, $isRight = false)
{
    $sum = 0;
    
    foreach ($arr as $x => $row) {
        $y = $isRight ? (count($arr) - ($x + 1)) : $x;
        $sum += $row[$y];
    }
    
    return $sum;
}

function diagonalDifference($arr) {
    return abs(diagSum($arr, false) - diagSum($arr, true));
}