function birthday(s, d, m) {
    let segmentCount = 0;
    let startIndex = 0;
    let barSlice;
    let barSum;
    
    while (m <= s.length) {
        barSlice = s.slice(startIndex, m);
        barSum = barSlice.reduce((prev, cur) => prev + cur);
        if (barSum == d) {
            segmentCount++;
        }
        startIndex++;
        m++;
    }
    
    return segmentCount;
}