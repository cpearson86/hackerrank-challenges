<?php
function birthdayCakeCandles($candles) {
    // Write your code here
    $tallestSize = max($candles);
    $tallestCount = 0;
    
    foreach ($candles as $candleSize) {
        if ($candleSize == $tallestSize) {
            $tallestCount++;
        }
    }
    
    return $tallestCount;
}