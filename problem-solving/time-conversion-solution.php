<?php
function timeConversion($s) {
    // Write your code here
    $hour = (int) substr($s, 0, 2);
    $minute = (int) substr($s, 3, 2);
    $second = (int) substr($s, 6, 2);
    $amPm = substr($s, 8, 2);
    
    if ($hour != 12 && $amPm == 'PM') {
        $hour += 12;
    } elseif ($hour == 12 && $amPm == 'AM') {
        $hour = 0;
    }
    
    return sprintf('%02u:%02u:%02u', $hour, $minute, $second);
}