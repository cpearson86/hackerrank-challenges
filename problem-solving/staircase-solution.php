<?php
function staircase($n) {
    // Write your code here
    for ($i = 1; $i <= $n; $i++) {
        $line = '';
        for ($j = 1; $j <= $i; $j++) {
            $line .= '#';
        }
        $line = str_pad($line, $n, ' ', STR_PAD_LEFT) . "\n";
        echo $line;
    }
}