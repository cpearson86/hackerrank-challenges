function breakingRecords(scores) {
    let minScore = scores.shift();
    let maxScore = minScore;
    let minRecordsBroken = 0;
    let maxRecordsBroken = 0;
    
    for (let key in scores) {
        if (scores[key] < minScore) {
            minRecordsBroken++;
            minScore = scores[key];
        } else if (scores[key] > maxScore) {
            maxRecordsBroken++;
            maxScore = scores[key];
        }
    }
    
    return [maxRecordsBroken, minRecordsBroken];
}