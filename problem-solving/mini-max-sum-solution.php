<?php
function miniMaxSum($arr) {
    // Write your code here
    sort($arr, SORT_NUMERIC);
    $smallestNums = array_slice($arr, 0, 4);
    $biggestNums = array_slice($arr, -4);
    
    printf("%u %u\n", array_sum($smallestNums), array_sum($biggestNums));
}