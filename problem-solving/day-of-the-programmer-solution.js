function getMonthLength(month, isLeapYear)
{
    let monthLength;
    
    switch (month) {
        case 2:
            monthLength = isLeapYear ? 29 : 28;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            monthLength = 30;
            break;
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            monthLength = 31;
            break;
    }
    
    return monthLength;
}

function dayOfProgrammer(year) {
    let yearLength;
    let isLeapYear;
    
    if (year >= 1918) {
        isLeapYear = ((year % 400) == 0) || (((year % 4) == 0) && ((year % 100) != 0));
    } else {
        isLeapYear = ((year % 4) == 0);
    }
    
    if (year == 1918) {
        yearLength = 352;
    } else {
        yearLength = isLeapYear ? 366 : 365;
    }
    
    let daysElapsed = yearLength;
    let month = 12;
    let monthLength;
    let day;
    
    while (daysElapsed > 256) {
        monthLength = getMonthLength(month, isLeapYear);
        daysElapsed -= monthLength;
        if (daysElapsed < 256) {
            day = 256 - daysElapsed;
        } else {
            month--;
        }
    }
    
    return `${String(day).padStart(2, '0')}.${String(month).padStart(2, '0')}.${year}`;
}