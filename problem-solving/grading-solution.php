<?php
function gradingStudents($grades) {
    return array_map(function($grade) {
        if ($grade >= 38) {
            $nextMultiple = (ceil($grade / 5) * 5);
            $difference = $nextMultiple - $grade;
            if ($difference < 3) {
                $grade = $nextMultiple;
            }
        }
        
        return $grade;
    }, $grades);
}