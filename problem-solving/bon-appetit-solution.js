function bonAppetit(bill, k, b) {
    const itemsToPay = bill.filter((element, index) => index != k);
    const owed = itemsToPay.reduce((previous, current) => previous + current) / 2;
    const overpaid = b - owed;
    
    process.stdout.write(overpaid <= 0 ? "Bon Appetit\n" : overpaid + "\n");
}