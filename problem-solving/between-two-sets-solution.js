function getTotalX(a, b) {
    let betweenCount = 0;
    let aKey;
    let bKey;
    
    const minimum = Math.min(...a);
    const maximum = Math.min(...b);
    
    candidateLoop: for (let candidate = minimum; candidate <= maximum; candidate += minimum) {
        for (aKey in a) {
            if ((candidate % a[aKey]) != 0) {
                continue candidateLoop;
            }
        }
        
        for (bKey in b) {
            if ((b[bKey] % candidate) != 0) {
                continue candidateLoop;
            }
        }
        
        betweenCount++;
    }
    
    return betweenCount;
}