function kangaroo(x1, v1, x2, v2) {
    const startPosDiff = x2 - x1;
    const jumpDiff = v1 - v2;
    
    return (startPosDiff / jumpDiff) >= 0 && (startPosDiff % jumpDiff) == 0 ? 'YES' : 'NO';
}