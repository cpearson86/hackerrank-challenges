function sockMerchant(n, ar) {
    let sockCounts = {};
    let pairCount = 0;
    for (let arrKey in ar) {
        if (sockCounts.hasOwnProperty(ar[arrKey])) {
            sockCounts[ar[arrKey]]++;
        } else {
            sockCounts[ar[arrKey]] = 1;
        }
    }
    
    for (let color in sockCounts) {
        pairCount += Math.floor(sockCounts[color] / 2);
    }
    
    return pairCount;
}