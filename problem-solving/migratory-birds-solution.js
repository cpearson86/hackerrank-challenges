function migratoryBirds(arr) {
    let counts = {};
    let birdId = Infinity;
    for (let arrKey in arr) {
        if (counts.hasOwnProperty(arr[arrKey])) {
            counts[arr[arrKey]]++;
        } else {
            counts[arr[arrKey]] = 1;
        }
    }
    
    let maxCount = Math.max(...Object.values(counts));
    for (let countKey in counts) {
        if (counts[countKey] == maxCount && countKey < birdId) {
            birdId = Number(countKey);
        }
    }
    return birdId;
}