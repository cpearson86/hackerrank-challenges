function divisibleSumPairs(n, k, ar) {
    let pairCount = 0;
    let pairSum;
    let endKey;
    for (let startKey = 0; startKey < (n - 1); startKey++) {
        for (endKey = startKey + 1; endKey < n; endKey++) {
            pairSum = ar[startKey] + ar[endKey];
            if (pairSum % k == 0) {
                pairCount++;
            }
        }
    }
    
    return pairCount;
}